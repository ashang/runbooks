{
  properties: {
    json: {
      properties: {
        jsonPayload: {
          properties: {
            err: {
              properties: {
                detail: {
                  type: 'object',
                  enabled: false,
                },
              },
            },
          },
        },
      },
    },
  },
}
